## Optimization problems using CPLEX

Practising optimization using the well-known CPLEX software.

<img src="/repo_icon.jpg" alt="drawing" width="400"/>

CPLEX has two optimization engines:
* The classic that is used to solved IPs, MIPs, etc.
* The Constrained Programming (CP) Optimizer Engine, specifically designed to solve scheduling problems efficiently. This engine adds to the OPL language a set of new variable types (interval, sequence, etc.), functions (step, pulse, etc.) and other tools that ease the formulation of scheduling problems. An introductory guide to the CP engine can be found in paper ["IBM ILOG CP optimizer for scheduling"](https://link.springer.com/epdf/10.1007/s10601-018-9281-x).


## Contents

[[1]](/Knapsack) 1-0 Knapsack problem (BILP).

[[2]](/JobSequencing_BILP_TimeIndexed) Job Sequencing problem with variable profits, durations and deadlines (BILP, time-indexed).

[[3]](/JobSequencing_CP) Job Sequencing problem with variable profits, durations and deadlines (CP).

## Slides

[Job Sequencing](https://docs.google.com/presentation/d/18Umi6LrH2UhwreSSmOJ5D1AHXY39cQPiOEn3LI7OPSY/edit?usp=sharing)

## Language

OPL (Optimization Programming Language).

## IDE

IBM ILOG CPLEX Optimization Studio.

## License

[MIT](https://choosealicense.com/licenses/mit/)
