## Job sequencing problem

Job sequencing problem with profits, durations and deadlines.

This problem has a nice dynamic programming solution. However, here we solve the problem using CPLEX via a Constraint Programming formulation
for the CP Optimizer.

__References__

[[1]](http://www.cs.mun.ca/~kol/courses/2711-f13/dynprog.pdf) Dynamic Programming Algorithms, Based on University of Toronto CSC 364 notes, original lectures by Stephen Cook.

## Problem formulation (CP Optimizer)

$`N`$: number of jobs

$`\mathcal{J}`$: set of jobs

$`P_j`$: profit of job $`j`$

$`L_j`$: length (in slots) of job $`j`$

$`D_j`$: deadline (in slots) of job $`j`$

$`T`$: maximum schedule length

Objective function:

```math
\text{maximize} \quad \sum_{j \in \mathcal{J}} P_j \cdot \text{presenceOf}(a_j)
```

subject to:

(jobs cannot overlap)

```math
\text{noOverlap}(\text{all}(j \in \mathcal{J}) a_j)
```

(jobs' deadline cannot be exceeded)

```math
\text{endOf}(a_j) \leq D_j, \quad \forall j \in \mathcal{J}
```

(interval varaibles)

```math
a_j \text{ optional interval of size } L_j, \quad \forall j \in \mathcal{J}
```

## Problem classification

This is a combinatorial optimization problem.

## Output example

Consider the next input data:

```c#
 N = 5; // Number of jobs
 T = 5; // Maximum schedule length
 
 L = [2 1 1 1 3]; 		// length/duration of each job
 P = [15 20 12 5 25]; 	// profit of each job
 D = [2 2 1 3 3]; 		// deadline of each job
```

Below is the generated output, with the value of the optimum and the associated interval variable.

Solutions:

```
// solution with objective 37
a = [<0 0 0 0>
         <1 1 2 1> <1 0 1 1> <1 2 3 1> <0 0 0 0>];

```

Engine log:

```
 ! --------------------------------------------------- CP Optimizer 20.1.0.0 --
 ! Maximization problem - 6 variables, 7 constraints
 ! Initial process time : 0.01s (0.01s extraction + 0.00s propagation)
 !  . Log search space  : 6.6 (before), 6.6 (after)
 !  . Memory usage      : 408.1 kB (before), 408.1 kB (after)
 ! Using parallel search with 8 workers.
 ! ----------------------------------------------------------------------------
 !          Best Branches  Non-fixed    W       Branch decision
                        0          6                 -
 + New bound is 77
 ! Using temporal relaxation.
                        0          5    1            -
 + New bound is 40
 *            25        5  0.03s        1      (gap is 60.00%)
 *            37       12  0.03s        1      (gap is 8.11%)
              37       43          2    1   F         !presenceOf(a(2))
 + New bound is 37 (gap is 0.00%)
 ! ----------------------------------------------------------------------------
 ! Search completed, 2 solutions found.
 ! Best objective         : 37 (optimal - effective tol. is 0)
 ! Best bound             : 37
 ! ----------------------------------------------------------------------------
 ! Number of branches     : 250
 ! Number of fails        : 147
 ! Total memory usage     : 3.8 MB (3.5 MB CP Optimizer + 0.4 MB Concert)
 ! Time spent in solve    : 0.03s (0.03s engine + 0.01s extraction)
 ! Search speed (br. / s) : 12,500.0
 ! ----------------------------------------------------------------------------
```

__Gantt Diagram__

CPLEX generates a Gantt diagram with the solution when you use CP Engine.

<img src="/JobSequencing_CP/gantt.PNG" alt="drawing" width="1000"/>

## Some toughts

This problem is formulated as an Constraint Programming (CP) problem following the particular OPL languague and models in CPLEX for the CP Optimizer Engine.

This CP Engine has been particularly designed to solve scheduling problems efficiently and to ease the modeling of them using variables like "interval" and functions like "presenceOf", "endOf". This is the right way to go with CPLEX for solving this problem and not solving it as a MILP.
We did that just by ilustration and learning purposes.
