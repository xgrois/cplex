/*********************************************
* Job sequencing with deadlines and variable durations
*
* This problem has a dynamic programming solution
* Here, however, we show how to solve the same problem
* using CPLEX
* To solve it, we describe the problem as a CP
* since CPLEX has a special CP Engine for scheduling
* problems
*
 *********************************************/

using CP;

int N = ...; // Number of jobs
int T = ...; // Max makespan

range Jobs = 1..N;

int D[Jobs] = ...; // Deadline

int L[Jobs] = ...; // Length, Duration

int P[Jobs] = ...; // Profits

// Job's presence as interval presence
// The presence of a job is optional, so the presence of an interval is also optional
dvar interval a[j in Jobs] optional in 0..(T) size L[j];

// If you maximize just the profit,
// the CP Engine will provide a solution where no job is scheduled
// becasue they are optional, and its presence or not does not affect
// the objective function. Actually, the objective function will be constant
// and always equal to sum(j in Jobs) P[j] either scheduled or not
//
// To associate the presence of an optional interval with the counting of its profit
// we can do it as follows

maximize sum(j in Jobs) (P[j] * presenceOf(a[j])); 

subject to {
  
  jobs_cannot_overlap:
  noOverlap(all(j in Jobs) a[j]);
  
  jobs_deadline_cannot_be_exceeded:
  forall (j in Jobs)
  	endOf(a[j]) <= D[j];
  
  }