## 0-1 Knapsack problem

Use CPLEX to solve the 0-1 Knapsack problem, i.e., maximize the value of the knapsack
by introducing a combination of elements that do not exceed maximum knapsack capacity.

<img src="/Knapsack/knapsack.jpg" alt="drawing" width="800"/>

__References__

[[1]](https://en.wikipedia.org/wiki/Knapsack_problem) Knapsack problem, Wikipedia.

## Problem formulation

We have $`N`$ items. Each item $`i`$ has a weigh $`w_i`$ and a value $`v_i`$.
We also have a knapsack with a limited capacity $`C`$.
The objective is to introduce in the knapsack the items that add highest value without exceeding the kanpsack capacity.

The binary variable $`x_i`$ help us to determine wheter we include item $`i`$ or not in the optimal solution.

```math
\max_{} \sum_{i = 1}^{N} v_i x_i
```

subject to:

```math
\sum_{i = 1}^{N} w_i x_i \leq C
```
```math
x_i \in \{0,1\}
```

## Problem classification

Combinatorial optimization.

This is an Integer Programming (IP) problem.

More precisely, since all variables are binary (0-1), this is a Binary Integer Programming (BIP).

In addition, it is also a Linear BIP, since the objective function is linear and the constraints are linear.

## Output example

Consider the next input data:

```c#
 N = 30;
 C = 30;
 
 v = [20, 5, 10, 40, 15, 25, 10, 20, 30, 5, 50, 10, 60, 50, 40, 34, 35, 20, 21, 22, 20, 5, 10, 40, 15, 25, 10, 20, 30, 5 ];
 w = [1, 2, 3, 8, 7, 4, 1, 2, 3, 4, 5, 6, 7, 7, 3, 4, 9, 1, 9, 15, 1, 2, 3, 8, 7, 4, 1, 2, 3, 4];
```

Below is the generated output, with the value of the optimum and the associated binary variables.

Solutions:

```
// solution (optimal) with objective 330
// Quality Incumbent solution:
// MILP objective                                 3.3000000000e+02
// MILP solution norm |x| (Total, Max)            1.20000e+01  1.00000e+00
// MILP solution error (Ax=b) (Total, Max)        0.00000e+00  0.00000e+00
// MILP x bound error (Total, Max)                0.00000e+00  0.00000e+00
// MILP x integrality error (Total, Max)          0.00000e+00  0.00000e+00
// MILP slack bound error (Total, Max)            0.00000e+00  0.00000e+00
// 

x = [1 0 0 0 0 0 1 1 1 0 1 0 1 0 1 0 0 1 0 0 1 0 0 0 0 0 1 1 1 0];
```

Engine log:

```
Version identifier: 20.1.0.0 | 2020-11-11 | 9bedb6d68
Legacy callback                                  pi
Found incumbent of value 0.000000 after 0.00 sec. (0.00 ticks)
Tried aggregator 1 time.
MIP Presolve eliminated 0 rows and 11 columns.
MIP Presolve added 5 rows and 5 columns.
Reduced MIP has 6 rows, 24 columns, and 39 nonzeros.
Reduced MIP has 9 binaries, 15 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (0.04 ticks)
Probing time = 0.00 sec. (0.00 ticks)
Tried aggregator 1 time.
Detecting symmetries...
MIP Presolve eliminated 3 rows and 3 columns.
MIP Presolve added 5 rows and 5 columns.
Reduced MIP has 8 rows, 26 columns, and 47 nonzeros.
Reduced MIP has 9 binaries, 17 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (0.03 ticks)
Probing time = 0.00 sec. (0.00 ticks)
MIP emphasis: balance optimality and feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.00 sec. (0.02 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

*     0+    0                            0.0000      702.0000              --- 
*     0+    0                          230.0000      702.0000           205.22%
*     0     0      integral     0      330.0000      330.0000        1    0.00%
Elapsed time = 0.02 sec. (0.14 ticks, tree = 0.00 MB, solutions = 3)

Root node processing (before b&c):
  Real time             =    0.02 sec. (0.14 ticks)
Parallel b&c, 8 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.02 sec. (0.14 ticks)

```

## Some toughts

Note that this problem can be solved using dynamic programming (DP), with pseudo-polynomial time complexity. We run this exact example using our code and the DP solution founded the optimum value faster than CPLEX (0ms vs 2ms).

Note, however, that the DP solution finds the optimum value, and do not return the value of the binary variables. It is generally possible to traceback the optimal allocation once we get the full DP table but it is an extra step after finding the optimum value.
