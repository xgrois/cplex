/*********************************************
 * OPL 20.1.0.0 Model
 * Author: jgrois
 * Creation Date: Dec 16, 2020 at 6:10:38 PM
 *********************************************/

 int N = ...; // number of items
 int C = ...; // Knapsack capacity
 
 range items = 1..N;
 
 int v[items] = ...; // value of each item
 int w[items] = ...; // weight of each item
 
 // decision variables
 dvar boolean x[items];
 
 // objective function
 maximize sum(i in items) v[i]*x[i];
 
 subject to{
   cons01:
   sum(i in items) w[i]*x[i] <= C;
 }
 
 
