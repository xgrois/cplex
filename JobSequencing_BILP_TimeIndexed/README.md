## Job sequencing problem

Job sequencing problem with profits, durations and deadlines.

This problem has a nice dynamic programming solution. However, here we solve the problem using CPLEX via an IP formulation.

__References__

[[1]](http://www.cs.mun.ca/~kol/courses/2711-f13/dynprog.pdf) Dynamic Programming Algorithms, Based on University of Toronto CSC 364 notes, original lectures by Stephen Cook.

## Problem formulation

$`N`$: number of jobs

$`P_j`$: profit of job $`j`$

$`L_j`$: length (in slots) of job $`j`$

$`D_j`$: deadline (in slots) of job $`j`$

$`T`$: maximum schedule length

Objective function:

```math
\text{maximize} \quad \sum_{j = 1}^{N} \sum_{t = 1}^{min(T,D_j)-L_j+1} P_{j} x_{jt}
```

subject to:

(a job cannot be scheduled more than once)
```math
\sum_{t = 1}^{min(T,D_j)-L_j+1} x_{jt} \leq 1, \quad \forall 1 \leq j \leq N
```
(jobs cannot overlap)
```math
\sum_{j = 1}^{N} \sum_{s = t-L_j+1}^{min(t,D_j)} x_{js} \leq 1, \quad \forall 1 \leq t \leq T
```
```math
x_{jt} \in \{0,1\}, \quad \forall 1 \leq j \leq N, \forall 1 \leq t \leq T
```

Jobs must be processed till completion (cannot partition jobs).

## Problem classification

This is a combinatorial optimization problem.

Here, it is formulated as an Integer Programming (IP) problem.
Note that there are many different ways to formulate this problem as an IP. Here we use the time-indexed formulation.
Depending on the specific formulation used, different techniques may be applied afterwards or different performance can be achived in IP solvers.

The schedule can be defined as a binary matrix (with entries $`x_{j,t}`$), so this is a Binary Integer Programming (BIP).

In addition, it is also a Linear BIP, since the objective function is linear and the constraints are linear (no quadratics, etc.).

## Output example

Consider the next input data:

```c#
 N = 6; // Number of jobs
 T = 5; // Maximum schedule length
 
 L = [1 1 1 2 1 2]; 		// length/duration of each job
 P = [10 60 20 50 30 10]; 	// profit of each job
 D = [5 3 1 3 2 2]; 		// deadline of each job
```

Below is the generated output, with the value of the optimum and the associated binary variables.

Solutions:

```
// solution (optimal) with objective 120
// Quality Incumbent solution:
// MILP objective                                 1.2000000000e+02
// MILP solution norm |x| (Total, Max)            3.00000e+00  1.00000e+00
// MILP solution error (Ax=b) (Total, Max)        0.00000e+00  0.00000e+00
// MILP x bound error (Total, Max)                0.00000e+00  0.00000e+00
// MILP x integrality error (Total, Max)          0.00000e+00  0.00000e+00
// MILP slack bound error (Total, Max)            0.00000e+00  0.00000e+00
// 

X =         [[0 0 0 1 0]
             [0 0 1 0 0]
             [0 0 0 0 0]
             [1 0 0 0 0]
             [0 0 0 0 0]
             [0 0 0 0 0]];

```

Engine log:

```
Version identifier: 20.1.0.0 | 2020-11-11 | 9bedb6d68
Legacy callback                                  pi
Found incumbent of value 0.000000 after 0.00 sec. (0.00 ticks)
Tried aggregator 1 time.
MIP Presolve eliminated 5 rows and 6 columns.
MIP Presolve modified 1 coefficients.
Reduced MIP has 6 rows, 10 columns, and 19 nonzeros.
Reduced MIP has 10 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (0.02 ticks)
Probing time = 0.00 sec. (0.00 ticks)
Tried aggregator 1 time.
MIP Presolve eliminated 1 rows and 3 columns.
MIP Presolve modified 1 coefficients.
Reduced MIP has 5 rows, 7 columns, and 14 nonzeros.
Reduced MIP has 7 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (0.01 ticks)
Probing time = 0.00 sec. (0.00 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 5 rows, 7 columns, and 14 nonzeros.
Reduced MIP has 7 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (0.01 ticks)
Probing time = 0.00 sec. (0.00 ticks)
Clique table members: 5.
MIP emphasis: balance optimality and feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.02 sec. (0.01 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

*     0+    0                            0.0000      310.0000              --- 
*     0+    0                          120.0000      310.0000           158.33%
      0     0      125.0000     5      120.0000      125.0000        4    4.17%
      0     0        cutoff            120.0000      125.0000        4    4.17%
Elapsed time = 0.02 sec. (0.09 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    0.02 sec. (0.09 ticks)
Parallel b&c, 8 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.02 sec. (0.09 ticks)

```

## Some toughts

This is just the example of how to formulate the job sequencing problem as an time-indexed IP and how to use CPLEX to solve it.

Note, however, that this problem sould not be solved using this approach in practice. IBM ILOG CPLEX Optimization Studio has a Constraint Problem Engine that is specific for solving scheduling problems like this. This would require a different model in CPLEX.
Hence, this problem should not be solved as an IP.

This is just for curiosity, learning and illustration purposes.
