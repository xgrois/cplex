/*********************************************
* Job sequencing with deadlines and variable durations
*
* This problem has a dynamic programming solution
* Here, however, we show how to solve the same problem
* using CPLEX
* To solve it, we describe the problem as an IP 
* since CPLEX can solve IPs
*
 *********************************************/

 int N = ...; // number of jobs
 int T = ...; // planning horizon
 
 range Jobs = 1..N;
 range Slots = 1..T;
 
 int L[Jobs] = ...; // length/duration of each job
 int P[Jobs] = ...; // profit of each job
 int D[Jobs] = ...; // deadline of each job
 
 // decision variables
 dvar boolean X[Jobs][Slots];
 
 // objective function
 maximize sum(j in Jobs, t in 1..minl(T,D[j])-L[j]+1) (X[j][t]*P[j]);
 
 subject to{
   cons01:
   forall (j in Jobs) sum(s in 1..minl(T,D[j])-L[j]+1) X[j][s] <= 1;
   
   forall (t in Slots)
   cons02:
   sum(j in Jobs, s in maxl(1,t-L[j]+1)..minl(t,D[j])) (X[j][s]) <= 1;
   
 }